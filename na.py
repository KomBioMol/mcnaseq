import gromologist as gml
import pmx
import numpy as np
from random import choice
from scipy.integrate import simps
from subprocess import call
import os
from glob import glob
from shutil import copy2, copytree
import argparse


class Nucleic:
    """
    A class that wraps all info about the pair of nucleic acids being
    simulated - one (_a) in a complex, and another (_b) free in solvent
    """
    def __init__(self, options, **extras):
        self.attempt_counter = 0
        self.max_cpu_per_sim = 48
        self.struct_a = options.struct_a
        self.struct_b = options.struct_b
        self.restarting = False
        self.restart_attempts = 0
        if len(glob('swap_*_a_0')) > 1:
            self.restart()
            self.restarting = True
        self.pmxff = options.pmxff
        self.extra_options = extras
        self.time = options.time
        self.ds = None
        self.seq = None
        self.attempt = None
        self.range = options.range
        self.n = options.n
        assert all([i == j for i, j in zip(gml.Pdb(self.struct_a).print_nucleic_sequence(),
                                           gml.Pdb(self.struct_b).print_nucleic_sequence())])
        self.read_seq(self.struct_a)

    def read_seq(self, struct):
        """
        Reads the DNA/RNA sequence of the 1st chain from the PDB file
        :param struct: str, path to the PDB file
        :return: None
        """
        pdb = gml.Pdb(struct)
        n_chains = len(pdb.print_nucleic_sequence())
        if n_chains == 1:
            self.ds = False
        elif n_chains == 2:
            self.ds = True
        else:
            raise RuntimeError("Number of NA chains is {}, currently only 1 or 2 are supported")
        self.seq = pdb.print_nucleic_sequence()[0]

    def run(self):
        """
        The main entry point for the script, an infinite loop that keeps
        alternating between swap attempts and post-attempt equilibration
        :return: None
        """
        while True:
            try:
                w, mins = self.swap_attempt()
                self.accept_step('swap', w, mins)
                self.eq_part()
                self.accept_step('eq', [0])
                self.read_seq(f'eq_{self.attempt_counter}_0_b/noalch.pdb')
                self.attempt_counter += 1
            except Exception as e:
                if self.attempt_counter > 5 and self.restart_attempts < 2:
                    self.restart()
                else:
                    raise e

    def restart(self):
        self.restart_attempts += 1
        self.attempt_counter = len(glob('swap_*_a_0'))//3 - 2
        self.accept_step('eq', 0)
        self.attempt_counter += 1
        self.restarting = True

    def swap_attempt(self):
        """
        Randomly chooses a base to modify, introduces the alchemical change
        in both systems and performs the fast-growth runs in the respective directories
        :return: float, dG difference between systems A and B
        """
        if not self.range:
            rng = list(range(2, len(self.seq)))
        else:
            rng = [int(x) for x in self.range.split('-')]
        to_swap = rng[self.attempt_counter % len(rng)]
        base_swap = self.seq[to_swap-1]
        wc_dict = {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C'}
        wcx_dict = {'A': 'TCG', 'T': 'ACG', 'C': 'GAT', 'G': 'CAT'}
        bases_new = wcx_dict[base_swap]
        multidirs = []
        dirs_all = []
        self.attempt = [f'{to_swap}{base_swap}->{base}' for base in base_swap + bases_new]
        for n, base_new in enumerate(bases_new):
            if self.ds:
                swapping = [to_swap, 2*len(self.seq) - to_swap + 1]
                names = ['D' + base_new, 'D' + wc_dict[base_new]]
            else:
                swapping = [to_swap]
                names = ['D' + base_new]
            dirs = [f'swap_{self.attempt_counter}_{n}_a_0', f'swap_{self.attempt_counter}_{n}_b_0',
                    f'swap_{self.attempt_counter}_{n}_a_1', f'swap_{self.attempt_counter}_{n}_b_1']
            for directory in dirs:
                self.mkdir(directory)
            self.mutate_system(self.struct_a, swapping, names, f'swap_{self.attempt_counter}_{n}_a_0')
            self.mutate_system(self.struct_b, swapping, names, f'swap_{self.attempt_counter}_{n}_b_0')
            self.mutate_system(self.struct_a, swapping, names, f'swap_{self.attempt_counter}_{n}_a_1')
            self.mutate_system(self.struct_b, swapping, names, f'swap_{self.attempt_counter}_{n}_b_1')
            dirs_all.extend(dirs)
            multidirs.append(self.multimkdir(n))
        self.run_job(dirs_all, 'mini')
        self.run_job(dirs_all, 'eq')
        self.run_job([d for multi in multidirs for d in multi], 'prod')
        return self.get_ddg(multidirs)

    def eq_part(self):
        """
        As self.swap_attempt, but performs a post-swap-attempt equilibration
        step of the same length as the attempt itself
        :return: None
        """
        dirs = [f'eq_{self.attempt_counter}_0_a', f'eq_{self.attempt_counter}_0_b']
        for directory in dirs:
            self.mkdir(directory)
        copy2(self.struct_a, f'{dirs[0]}/conf.pdb')
        copy2(self.struct_b, f'{dirs[1]}/conf.pdb')
        self.copytree(self.pmxff, f'{dirs[0]}/' + self.pmxff.split('/')[-1])
        self.copytree(self.pmxff, f'{dirs[1]}/' + self.pmxff.split('/')[-1])
        nhis = self.count_his(self.struct_a)
        call(f'{nhis} mpiexec -n 1 gmx_mpi pdb2gmx -f {dirs[0]}/conf.pdb -ff {self.pmxff.replace(".ff", "")} '
             f'-water tip3p -o {dirs[0]}/conf.pdb -p {dirs[0]}/topol.top -ignh -his >> mcna.log 2>&1', shell=True)
        _ = [os.remove(i) for i in glob('*posre*')]
        call(f'{nhis} mpiexec -n 1 gmx_mpi pdb2gmx -f {dirs[1]}/conf.pdb -ff {self.pmxff.replace(".ff", "")} '
             f'-water tip3p -o {dirs[1]}/conf.pdb -p {dirs[1]}/topol.top -ignh -his >> mcna.log 2>&1', shell=True)
        _ = [os.remove(i) for i in glob('*posre*')]
        self.run_job(dirs, 'mini')
        self.run_job(dirs, 'eq')
        self.run_job(dirs, 'prod')

    def mutate_system(self, pdb, resids, target_names, directory):
        """
        The main "alchemical" function that introduces the mutation into the PDB,
        runs pdb2gmx with the mutated structure, and updates the topology with
        alchemical parameters
        :param pdb: str, the PDB file to be mutated
        :param resids: list of int, numbers of residues to mutate
        :param target_names: list of str, names of residues to be introduced
        :param directory: str, the directory in which the mutation will be performed
        :return: None
        """
        m = pmx.model.Model(pdb, rename_atoms=True)
        for res, name in zip(resids, target_names):
            print(m, res, name, self.pmxff)
            m = pmx.alchemy.mutate(m=m, mut_resid=res, mut_resname=name, ff=self.pmxff)
        m.write('/'.join([directory, 'conf.pdb']))
        copy2(f'{self.pmxff}/residuetypes.dat', './residuetypes.dat')
        self.copytree(self.pmxff, directory + '/' + self.pmxff.split('/')[-1])
        call(f'mpiexec -n 1 gmx_mpi pdb2gmx -f {directory}/conf.pdb -ff {self.pmxff.replace(".ff", "")} -water tip3p '
             f'-o {directory}/conf.pdb -p {directory}/topol.top >> mcna.log 2>&1', shell=True)
        _ = [os.remove(i) for i in glob('*posre*')]
        topol = pmx.forcefield.Topology(f'{directory}/topol.top', ff=self.pmxff)
        pmxtop, pmxitps = pmx.alchemy.gen_hybrid_top(topol)
        pmxtop.write('/'.join([directory, 'topol.top']))
        for pmxitp in pmxitps:
            for atom in pmxitp.atoms:
                atom.name = atom.name.replace('DH', 'Hx').replace('DO', 'Ox').replace('DN', 'Nx').replace('DC', 'Cx').replace('DS', 'Sx')
            pmxitp.write('/'.join([directory, f'topol_{pmxitp.name}.itp']))
        self.remove_duplicates('/'.join([directory, 'topol.top']))

    @staticmethod
    def remove_duplicates(filename):
        """
        Removes erroneous entries from pmx-generated .tops
        (easier done here while waiting for a fix)
        :param filename: str, path to a .top file
        :return: None
        """
        fl = open(filename, 'r')
        processed = []
        for line in fl.readlines():
            if not (line in processed and line.lstrip().startswith('#')):
                processed.append(line)
        with open(filename, 'w') as out:
            out.write(''.join(processed))

    def make_mdp(self, dir, runtype, stride=20, lincs='h-bonds', temperature=300, back=False):
        """
        Generates the .mdp file for each type of run
        :param runtype: str, can be 'mini', 'eq' or 'prod'
        :param stride: int, how often to dump frames to .xtc (in ps)
        :param lincs: str, whether to constrain h-bonds or all-bonds
        :param temperature: float, temperature for the run
        :return: None
        """
        trr = 2500
        nst = 500 * self.time
        if runtype == 'eq':
            trr = 500
            nst = 500 + self.n//2 * trr
        mdp_defaults = {"nstcomm": 100, "nstenergy": 5000, "nstlog": 5000, "nstcalcenergy": 100,
                        "nstxout-compressed": 500 * stride, "compressed-x-grps": "System",
                        "compressed-x-precision": trr, "free-energy": "yes", "sc-alpha": 0.3, "sc-coul": "yes",
                        "sc-sigma": 0.25, "dt": 0.002, "constraints": lincs, "coulombtype": "PME",
                        "tcoupl": "v-rescale", "ref-t": temperature, "tau-t": 0, "ref-p": 1.0,
                        "rlist": 1.2, "rcoulomb": 1.2, "vdw-type": "Cut-off", "rvdw_switch": 0.8, "rvdw": 1.2,
                        "compressibility": "4.5e-5", "tau-p": 1.0, "tc-grps": "System"}
        mini_defaults = {"integrator": "steep", "free-energy": "yes", "sc-alpha": 0.3, "sc-coul": "yes",
                         "sc-sigma": 0.25, "nsteps": 200, "emtol": 200, "emstep": 0.001, "nstlist": 10,
                         "pbc": "xyz", "coulombtype": "PME", "vdw-type": "Cut-off"}
        if runtype == 'eq':
            extra_defaults = {"gen-vel": "yes", "gen-temp": temperature, "pcoupl": "Berendsen",
                              "separate-dhdl-file": "no", "nsteps": nst, "nstxout": trr, "nstvout": trr}
        else:
            extra_defaults = {"gen-vel": "no", "pcoupl": "Parrinello-Rahman", "separate-dhdl-file": "yes",
                              "nsteps": nst, "nstxout": nst, "nstvout": nst}
        mdp_defaults.update(extra_defaults)
        mdp_defaults.update(self.extra_options)
        default = mini_defaults if runtype == 'mini' else mdp_defaults
        dl = 0 if runtype in ['eq', 'mini'] else 1 / nst
        init, sign = (1, -1) if back else (0, 1)
        default.update({"init-lambda": init, "delta-lambda": sign*dl})
        mdp = '\n'.join(["{} = {}".format(param, value) for param, value in default.items()]) + '\n'
        with open('{}/run.mdp'.format(dir), 'w') as outfile:
            outfile.write(mdp)

    def run_job(self, dirs, runtype):
        """
        Runs Gromacs (both grompp and mdrun)
        :param dirs: list of str, where the runs should be performed
        :param runtype: str, type of run to be performed (can be 'mini', 'eq' or 'prod')
        :return: None
        """
        out = 'eq' if runtype == 'eq' else 'mini' if runtype == 'mini' else 'dyn'
        conf = 'mini' if runtype == 'eq' else 'conf' if runtype == 'mini' else 'eq'
        top = 'topol.top'
        for n, d in enumerate(dirs):
            if "a_1" in d or "b_1" in d:
                self.make_mdp(d, runtype, back=True)
            else:
                self.make_mdp(d, runtype)
            base_d = d.split("/")[0]
            if runtype == 'prod':
                if 'swap' in d:
                    trr = f' -t {base_d}/eq.trr -time {1 + int(d.split("/")[-1].replace("run", ""))} '
                else:
                    trr = f' -t {base_d}/eq.trr -time {1 + self.n//2} '
            else:
                trr = ''
            call(f'mpiexec -n 1 gmx_mpi grompp {trr} -f {d}/run.mdp -p {base_d}/{top} -c {base_d}/{conf}.pdb -o '
                 f'{d}/{out} -maxwarn 3 >> mcna.log 2>&1', shell=True)
        n_cpu = int(os.environ["SLURM_NPROCS"])
        max_cpus = len(dirs) * self.max_cpu_per_sim
        if max_cpus < n_cpu:
            mpie = f"-n {max_cpus}"
        else:
            mpie = ''
        call(f'mpiexec {mpie} gmx_mpi mdrun -deffnm {out} -c {out}.pdb -multidir {" ".join(dirs)} -v >> mcna.log 2>&1',
             shell=True)

    @staticmethod
    def calc_work(xvg, total_time):
        """
        Integrates the dH/dlambda term from dhdl.xvg in Gromacs
        to calculate the total work performed in a pulling
        :param xvg: str, file to the .xvg file
        :param total_time: float, the final time expected in the .xvg file
        :return: float, the calculated value of work
        """
        dhdl = np.loadtxt(xvg, comments=['#', '@'])
        if not int(dhdl[-1, 0]) == int(total_time):
            raise RuntimeError(f"In file {xvg} last line reads {dhdl[-1]}, less than the requested total "
                               f"fime of {total_time} ps")
        dhdl_dict = {i: j for i, j in dhdl}
        dhdl = np.array([dhdl_dict[i] for i in sorted(list(dhdl_dict.keys()))])  # avoid duplicates in case of restarts
        return simps(dhdl, np.linspace([0], [1], len(dhdl)).reshape(-1))

    def mc_accept(self, work):
        """
        The Monte Carlo step based on the difference of works in the two systems;
        also does logging of the sequence
        :param work: float, the work difference between the 2 systems
        :return: bool, whether to accept the step
        """
        prob = np.min([np.exp(-work/2.949), 1])
        accept = True if np.random.random() < prob else False
        q = 'not ' if not accept else ''
        if work != 0:
            with open('seq.log', 'a') as out:
                out.write(f'iteration {self.attempt_counter}, sequence: {self.seq}, attempting move {self.attempt}, '
                          f'work diff = {work}, {q}accepted\n')
        return accept

    def plain_accept(self, works):
        if works[0] != 0:
            with open('seq.log', 'a') as out:
                accept = np.argmin(works) + 1 if np.min(works) < 0 else 0
                out.write(f'iteration {self.attempt_counter}, sequence: {self.seq}, work diffs = {works}\n, '
                          f'choosing {self.attempt[accept]}')
        if np.min(works) > 0:
            return 0
        else:
            return np.argmin(works) + 1

    def multimkdir(self, base_num):
        dirs = []
        for i in range(self.n//2):
            for j in 'ab':
                for k in '01':
                    self.mkdir(f'swap_{self.attempt_counter}_{base_num}_{j}_{k}/run{i}')
                    dirs.append(f'swap_{self.attempt_counter}_{base_num}_{j}_{k}/run{i}')
        return dirs

    @staticmethod
    def mkdir(dirname):
        """
        A wrapper that avoids the 'directory exists' error
        :param dirname: str, dir to be created
        :return: None
        """
        if not os.path.isdir(dirname):
            os.mkdir(dirname)

    @staticmethod
    def copytree(source, target):
        """
        A wrapper that avoids the 'directory exists' error
        :param source: str, dir to be copied
        :param target: str, target path for the directory
        :return: None
        """
        if not os.path.isdir(target):
            copytree(source, target)

    def get_ddg(self, dirs_list):
        ddgs = []
        minimal = []
        for dirs in dirs_list:
            works_a0, works_a1, works_b0, works_b1 = [], [], [], []
            for d in dirs:
                w = self.calc_work(f'{d}/dyn.xvg', self.time)
                if '_a_0' in d:
                    works_a0.append(w)
                elif '_a_1' in d:
                    works_a1.append(-w)
                elif '_b_0' in d:
                    works_b0.append(w)
                else:
                    works_b1.append(-w)
            # dga = np.mean(works_a) - np.std(works_a) / (2 * 2.494)
            # dgb = np.mean(works_b) - np.std(works_b) / (2 * 2.494)
            dga = self.solve_bar(works_a0, works_a1, 0.5 * (np.mean(works_a0) + np.mean(works_a1)))
            dgb = self.solve_bar(works_b0, works_b1, 0.5 * (np.mean(works_b0) + np.mean(works_b1)))
            ddgs.append(dga - dgb)
            minimal.append((np.argmin(works_a0), np.argmin(works_b0)))
        return ddgs, minimal

    @staticmethod
    def solve_bar(w0, w1, guess, temperature=300):
        w0 = np.array(w0)
        w1 = np.array(w1)
        from scipy.optimize import minimize
        kbt = 2.494 * temperature / 300

        def fun_ssq(f, work0, work1, beta):
            ni = len(work0)
            nj = len(work1)
            logf = np.log(ni / nj)
            term1 = np.sum(1 / (1 + np.exp(logf + beta * work0 - beta * f)))
            term2 = np.sum(1 / (1 + np.exp(-logf - beta * work1 + beta * f)))
            return (term1 - term2) ** 2

        opt = minimize(fun_ssq, guess, args=(w0, w1, 1/kbt), options={'disp': False}, tol=0.0000001)
        return opt.x[0]

    def count_his(self, pdbfile):
        count = 0
        for line in open(pdbfile).readlines():
            if 'CA' in line and any([x in line for x in ['HIS', 'HSD', 'HSE', 'HSP', 'HID', 'HIE']]):
                count += 1
        return 'echo ' + '1 ' * count + '| ' if count else ''

    def accept_step(self, prefix, work, mins=None):
        """
        Accepts or rejects the MC step and removes all alchemical
        residues to prepare the system for another round of MC
        :param prefix: str, prefix for the directory ('eq' or 'swap')
        :param work: list of float, work differences
        :return: None
        """
        drop_a_res = {"DAC": "DC", "DAG": "DG", "DAT": "DT", "DCA": "DA", "DCG": "DG", "DCT": "DT", "DGA": "DA",
                      "DGC": "DC", "DGT": "DT", "DTA": "DA", "DTC": "DC", "DTG": "DG"}
        drop_a_chg = {"DAG": {'N6': 'O6'}, "DCT": {'N4': 'O4'}, "DGA": {'O6': 'N6'}, "DTC": {'O4': 'N4'}}
        drop_b_res = {"DAC": "DA", "DAG": "DA", "DAT": "DA", "DCA": "DC", "DCG": "DC", "DCT": "DC", "DGA": "DG",
                      "DGC": "DG", "DGT": "DG", "DTA": "DT", "DTC": "DT", "DTG": "DT"}
        accept = self.plain_accept(work)
        if accept:
            accept -= 1
            if prefix != 'swap':
                ext_dir_a = ext_dir_b = ''
            elif mins is not None:
                ext_dir_a = f'run{mins[accept][0]}/'
                ext_dir_b = f'run{mins[accept][1]}/'
            else:
                ext_dir_a = ext_dir_b = 'run0/'
            suffix = "_0" if prefix == 'swap' else ''
            top_a = gml.Top(f'{prefix}_{self.attempt_counter}_{accept}_a{suffix}/topol.top',
                            pdb=f'{prefix}_{self.attempt_counter}_{accept}_a{suffix}/{ext_dir_a}dyn.pdb',
                            ignore_ifdef=True)
            top_b = gml.Top(f'{prefix}_{self.attempt_counter}_{accept}_b{suffix}/topol.top',
                            pdb=f'{prefix}_{self.attempt_counter}_{accept}_b{suffix}/{ext_dir_b}dyn.pdb',
                            ignore_ifdef=True)
            top_a.drop_state_a(remove_dummies=True)
            top_b.drop_state_a(remove_dummies=True)
            for a in top_a.pdb.atoms + top_b.pdb.atoms:
                if a.atomname.startswith('D') or a.atomname[:2] in ['1D', '2D', '3D'] \
                        or (len(a.atomname) > 1 and a.atomname[1] == 'x'):
                    a.atomname = a.atomname.replace('D', '').replace('x', '')
                if a.resname in drop_a_chg.keys() and a.atomname in drop_a_chg[a.resname]:
                    a.atomname = drop_a_chg[a.resname][a.atomname]
                if a.resname in drop_a_res.keys():
                    a.resname = drop_a_res[a.resname]
        else:
            suffix = "_0" if prefix == 'swap' else ''
            top_a = gml.Top(f'{prefix}_{self.attempt_counter}_{accept}_a{suffix}/topol.top',
                            pdb=f'{prefix}_{self.attempt_counter}_{accept}_a{suffix}/eq.pdb', ignore_ifdef=True)
            top_b = gml.Top(f'{prefix}_{self.attempt_counter}_{accept}_b{suffix}/topol.top',
                            pdb=f'{prefix}_{self.attempt_counter}_{accept}_b{suffix}/eq.pdb', ignore_ifdef=True)
            top_a.drop_state_b(remove_dummies=True)
            top_b.drop_state_b(remove_dummies=True)
            for a in top_a.pdb.atoms + top_b.pdb.atoms:
                if a.resname in drop_b_res.keys():
                    a.resname = drop_b_res[a.resname]
        top_a.pdb.add_chains(maxwarn=-1)
        top_b.pdb.add_chains(maxwarn=-1)
        top_a.pdb.save_pdb(f'{prefix}_{self.attempt_counter}_{accept}_a{suffix}/noalch.pdb')
        top_b.pdb.save_pdb(f'{prefix}_{self.attempt_counter}_{accept}_b{suffix}/noalch.pdb')
        self.struct_a = f'{prefix}_{self.attempt_counter}_{accept}_a{suffix}/noalch.pdb'
        self.struct_b = f'{prefix}_{self.attempt_counter}_{accept}_b{suffix}/noalch.pdb'


if __name__ == "__main__":
    def parse_args():
        parser = argparse.ArgumentParser()
        parser.add_argument('-f', type=str, dest='struct_a', default='',
                            help='a structure file with the initial structure')
        parser.add_argument('-g', type=str, dest='struct_b', default='',
                            help='a structure file with the reference state (free NA)')
        parser.add_argument('--plumed', type=str, dest='plumed', default='',
                            help='a plumed input to use during equilibration')
        parser.add_argument('--plumed2', type=str, dest='plumed2', default='',
                            help='a plumed input to use during equilibration for state B, optional')
        parser.add_argument('--pmxff', type=str, dest='pmxff', default='',
                            help='full path to the pmx FF directory')
        parser.add_argument('-t', type=int, dest='time', default=100,
                            help='time of a single step (MC swap attempt as well as equilibration)')
        parser.add_argument('-n', type=int, dest='n', default=12,
                            help='number of forward trajectories to run')
        parser.add_argument('--range', type=str, dest='range', default='',
                            help='range of indices (inclusive, 1-based) of nucleic acid residues to be optimized, e.g. '
                                 '3-6 corresponds to residues 3, 4, 5 and 6')
        arguments, unknown = parser.parse_known_args()
        return arguments, {x: y for x, y in [q.split('=') for q in unknown if q.count('=') == 1]}

    args, extra = parse_args()
    nucl = Nucleic(args, **extra)
    nucl.run()
